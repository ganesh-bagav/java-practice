public class AssigningDataToArray {
    public static void main(String[] args) {

        char [] arr = new char[66];
        arr[0]='a';
        arr[1]='b';
        arr['A']=65;

        int [] arr1 = new int[66];
        // float [] arr1 = new float[66]; same result with point using float
        arr1[0]=1;
        arr1[1]='A';
        arr1['A']=3;
        
        System.out.println(arr[0]);     //a
        System.out.println(arr[1]);     //b
        System.out.println(arr['A']);   //A
        System.out.println(arr1[0]);    //1
        System.out.println(arr1[1]);    //65
        System.out.println(arr1[65]);   //3
    }
}
