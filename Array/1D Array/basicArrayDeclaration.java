class basicArrayDeclaration {
    public static void main(String[] args) {
        
        int arr[];      // only initialize int array that don't have any physical space 
                        // But later on we can use tha initialized variable to assign 
                        // memory  using 'new' ex. arr = new int[3];
        arr = new int[3];

        // int arr1[3]; // Error : because what if we only initialize 3 size integer array
                        // which is 12 bytes and don't use it , then this space will be useless 
                        // and jvm is very strict about the space
        
        
        int arr2[] = {1,2,3};
        // int arr2[3] = {1,2,3}; // Error : we cant give size to a array and initializer list at the same time

        int arr3[] = new int[3] ; // give memory on heap section
        int arr4[] = new int[]{1,2,3} ; // give memory on heap section using initializer list
        // int arr3[] = new int[3]{1,2,3} ; // Error : array creation with both dimension expression and initialization is illegal


    }
}
