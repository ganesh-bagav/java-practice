import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class AssigningDataToArray2 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter Array Size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i = 0 ; i<size ;     i++){

            System.out.println("Enter Array Data :");
            arr[i]=Integer.parseInt(br.readLine());
        }

        for(int i=0 ; i<size ; i++){
            System.out.println(arr[i]);
        }
    }
}
