// BufferedReader Example

import java.io.*;
// import java.io.BufferedReader;
// import java.io.InputStreamReader;
// import java.io.IOException;
// import java.lang.Integer;
// import java.lang.Float;

class p1 {
    public static void main(String[] args) throws IOException {
        System.out.println("main");
        InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(ir);

        System.out.println("int br.read()");
        int n = br.read();
        br.skip(1);
        System.out.println("char br.read()");
        char ch = (char) br.read();
        System.out.print(ch);

        System.out.print("readLine");
        String temp = br.readLine(); // used to clear the stream of bufferedReader otherwise it will cause
                                     // java.lang.NumberFormatException:
        System.out.print(temp);

        int i = Integer.parseInt(br.readLine());
        float f = Float.parseFloat(br.readLine());
        String str = br.readLine();
        System.out.println(n);
        System.out.println(i);
        System.out.println(f);
        System.out.println(str);

    }
}
