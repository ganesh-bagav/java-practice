import java.util.StringTokenizer;

class StringToken{

    public static void main(String [] args){
        StringTokenizer st = new StringTokenizer("Ganesh Bagav. gg ,. ff",", ");
        while(st.hasMoreTokens()){ // without checking for next token we accessing it , cause java.util.NoSuchElementException
            System.out.println(st.nextToken().trim());
        }
    }

}
