import java.util.Scanner;

public class p2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String str1 = sc.next(); // 1) ganesh bagav jj  2) ganesh bagav
        System.out.println(str1);// 1) ganesh           2) ganesh
        
        /* 
        int x= sc.nextInt();     -->    input of first next() method is pending in the InputStream pipe
                                        and when sc.nextInt() goes to take input it founds the pending string
                                        "bagav" in the pipe it takes that and try to convert it into integer
                                        and give input mismatch exception
        */
                                            
        String str2 = sc.next();
        System.out.println(str2);// 1) bagav            2)bagav

        String str3 = sc.next();
        System.out.println(str3);// 1) jj               2) (waiting for input...)

    }
}
