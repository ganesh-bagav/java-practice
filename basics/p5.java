class demo5{
	public static void main (String[]args){

		int a= 2;
		char ch = 65;
		if(0<1){
			System.out.println(" m1 "+a+" "+ch);
		}

		switch(a){
			case  2 :
				int y=1; // we can intiallize value inside switch
					 // but in c language we need to give { } brackets 
					 // to the switch case for intialize some variable 	
				System.out.println(" m1 ");
				break;
			
				// examle of writing OR switch case ( || )
				// if we want to write multiple switch case for 
				// same result we can write like y=this
			case 3 :  
			case 4 :
				System.out.println(" 1 ");
		}
	}
}
