// Assignment operator 
// += --> x += y : addition of x and y stored in x 
// -= --> x -= y : substraction of x-y stored in x
// =- --> x =- y : this operator is used to change sign of a variable it can 
// 		   convert positive value to negative and negative value to
// 		   positive if now y value is positive it will convert it into 
// 		   negative and store in x and wise versa  

class demo6{
	public static void main( String [] args ){
		int x =- 3;
		int y = 4;
		y =- x;
		int z =0;

		System.out.println(x+" "+y+" "+z);
	}
}
